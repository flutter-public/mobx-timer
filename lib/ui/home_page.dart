import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:timermobx/timer/timer_mobx.dart';
import 'package:timermobx/utils.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import '../timer/timer_mobx.dart';
import 'package:flutter/src/widgets/framework.dart';
import '../utils.dart';
import 'example_page.dart';
/// Example 05
final customWidth05 =
    CustomSliderWidths(trackWidth: 4, progressBarWidth: 20, shadowWidth: 40);
final customColors05 = CustomSliderColors(
    dotColor: HexColor('#FFB1B2'),
    trackColor: HexColor('#E9585A'),
    progressBarColors: [HexColor('#FB9967'), HexColor('#E9585A')],
    shadowColor: HexColor('#FFB1B2'),
    shadowMaxOpacity: 0.05);
final info05 = InfoProperties(
    topLabelStyle: TextStyle(
        color: Colors.white, fontSize: 16, fontWeight: FontWeight.w400),
    topLabelText: 'Elapsed',
    bottomLabelStyle: TextStyle(
        color: Colors.white, fontSize: 16, fontWeight: FontWeight.w400),
    bottomLabelText: 'time',
    mainLabelStyle: TextStyle(
        color: Colors.white, fontSize: 50.0, fontWeight: FontWeight.w600),
    modifier: (double value) {
      final time = printDuration(Duration(seconds: value.toInt()));
      return '$time';
    });
final CircularSliderAppearance appearance05 = CircularSliderAppearance(
    customWidths: customWidth05,
    customColors: customColors05,
    infoProperties: info05,
    startAngle: 270,
    angleRange: 360,
    size: 350);
final viewModel05 = ExampleViewModel(
    appearance: appearance05,
    min: 0,
    max: 59,
    pageColors: [Colors.black, Colors.black87]);


class HomePage extends StatelessWidget {
  final TimerMobX timer = TimerMobX();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        children: <Widget>[
          Observer(
            builder: (_) => Container(
              height: 200,
              child: timer.state ==  StateCounter.alarm ? Icon(Icons.notifications_active, color: Colors.yellow,size: 100,) : Container(),
            ),
          ),
          ExamplePage(viewModel: viewModel05, timer: timer),
          Observer(
            builder: (_) => Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[getButton(timer)],
            ),
          )
        ],
      ),
    );
  }
}


Widget getButton(TimerMobX timer) {

  if(timer.state == StateCounter.ready)
    return buttonStart(timer);

  if(timer.state == StateCounter.running){
    return Row(
      children: <Widget>[
        buttonPause(timer),
        buttonReset(timer)
      ],
    );
  }
  if(timer.state == StateCounter.paused){
    return Row(
      children: <Widget>[
        buttonStart(timer),
        buttonReset(timer)
      ],
    );
  }
  if(timer.state == StateCounter.alarm)
    return buttonReset(timer);
  return Container();
}

Widget buttonStart(TimerMobX timer) {
  return Padding(
    padding: const EdgeInsets.all(30),
    child: CircleAvatar(
      radius: 30,
      backgroundColor: Colors.blueAccent,
      child: IconButton(
        icon: Icon(
          Icons.play_arrow,
          color: Colors.white,
        ),
        onPressed: () {
          timer.timerStarted();
        },
      ),
    ),
  );
}

Widget buttonPause(TimerMobX timer) {
  return Padding(
    padding: const EdgeInsets.all(30),
    child: CircleAvatar(
      radius: 30,
      backgroundColor: Colors.blueAccent,
      child: IconButton(
        icon: Icon(
          Icons.pause,
          color: Colors.white,
        ),
        onPressed: () {
          timer.timerPause();
        },
      ),
    ),
  );
}

Widget buttonReset(TimerMobX timer) {
  return Padding(
    padding: const EdgeInsets.all(30),
    child: CircleAvatar(
      radius: 30,
      backgroundColor: Colors.blueAccent,
      child: IconButton(
        icon: Icon(
          Icons.replay,
          color: Colors.white,
        ),
        onPressed: () {
          timer.timerReset();
        },
      ),
    ),
  );
}

