import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:timermobx/timer/timer_mobx.dart';
class ExampleViewModel {
  final List<Color> pageColors;
  final CircularSliderAppearance appearance;
  final double min;
  final double max;
  final InnerWidget innerWidget;

  ExampleViewModel(
      {
        @required this.pageColors,
      @required this.appearance,
      this.min = 0,
      this.max = 59,
      this.innerWidget,
      });
}

class ExamplePage extends StatelessWidget {
  final ExampleViewModel viewModel;
  final TimerMobX timer;
  const ExamplePage({
    Key key,
    @required this.viewModel,
    this.timer
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_)=> Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: viewModel.pageColors,
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  tileMode: TileMode.clamp)),
          child: Container(
            child: Center(
                child: SleekCircularSlider(
                    onChangeEnd: (double value) {
                      timer.changeTimer(value.floor());
                    },
              innerWidget: viewModel.innerWidget,
              appearance: viewModel.appearance,
              min: viewModel.min,
              max: viewModel.max,
              initialValue: timer.count_down.toDouble()
            )),
          ),
        ),
    );
  }
}
