// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timer_mobx.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TimerMobX on _TimerMobX, Store {
  final _$count_downAtom = Atom(name: '_TimerMobX.count_down');

  @override
  int get count_down {
    _$count_downAtom.reportRead();
    return super.count_down;
  }

  @override
  set count_down(int value) {
    _$count_downAtom.reportWrite(value, super.count_down, () {
      super.count_down = value;
    });
  }

  final _$stateAtom = Atom(name: '_TimerMobX.state');

  @override
  StateCounter get state {
    _$stateAtom.reportRead();
    return super.state;
  }

  @override
  set state(StateCounter value) {
    _$stateAtom.reportWrite(value, super.state, () {
      super.state = value;
    });
  }

  final _$_TimerMobXActionController = ActionController(name: '_TimerMobX');

  @override
  void timerReset() {
    final _$actionInfo =
        _$_TimerMobXActionController.startAction(name: '_TimerMobX.timerReset');
    try {
      return super.timerReset();
    } finally {
      _$_TimerMobXActionController.endAction(_$actionInfo);
    }
  }

  @override
  void timerPause() {
    final _$actionInfo =
        _$_TimerMobXActionController.startAction(name: '_TimerMobX.timerPause');
    try {
      return super.timerPause();
    } finally {
      _$_TimerMobXActionController.endAction(_$actionInfo);
    }
  }

  @override
  void timerStarted() {
    final _$actionInfo = _$_TimerMobXActionController.startAction(
        name: '_TimerMobX.timerStarted');
    try {
      return super.timerStarted();
    } finally {
      _$_TimerMobXActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeTimer(dynamic time) {
    final _$actionInfo = _$_TimerMobXActionController.startAction(
        name: '_TimerMobX.changeTimer');
    try {
      return super.changeTimer(time);
    } finally {
      _$_TimerMobXActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
count_down: ${count_down},
state: ${state}
    ''';
  }
}
