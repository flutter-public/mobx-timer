import 'dart:async';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:mobx/mobx.dart';
import 'package:timermobx/utils.dart';
part 'timer_mobx.g.dart';

class TimerMobX = _TimerMobX with _$TimerMobX;

abstract class _TimerMobX with Store {
  Timer _counterTimer;

  @observable
  int count_down = 0;

  @observable
  StateCounter state = StateCounter.reset;


  @action
  void timerReset() {
    count_down = 0;
    _counterTimer.cancel();
    state= StateCounter.reset;
    FlutterRingtonePlayer.stop();
  }

  @action
  void timerPause() {
    state = StateCounter.paused;
    _counterTimer.cancel();
  }

  @action
  void timerStarted() {
    state = StateCounter.running;
    _counterTimer = Timer.periodic(Duration(seconds: 1), (time) {
      if (count_down >= 1) {
        count_down--;
      }
      print(count_down);
      if (count_down == 0) {
        _counterTimer.cancel();
        state= StateCounter.alarm;
        FlutterRingtonePlayer.playNotification();
      }
    });
  }

  @action
  void changeTimer(time) {
    if(state == StateCounter.reset){
      state = StateCounter.ready;
    }
      count_down = time;
  }


}
